Last update:2015/06/21
Author: Nathaniel Chen, EMP Chen @ NCKU NEAT.
Email: ar801112usase@hotmail.com


# 程式的開頭MOTD是在 http://www.kammerl.de/ascii/AsciiSignature.php 網站做的，字體是banner3-D。
# 請記得執行前檢查config_ServerIPList.py內，MQTT broker的位置跟port是否正確。
# 如果有少甚麼套件，請不要問作者...問問pip或者萬能google，99.99%都有解，除非你不小心砍掉本專案的什麼檔案。
# 規則表(xxxRule.py)前面的List可以參考檔案內的註解做修改mapping.
# 主要單元執行檔案IoTServer.py, M2MFunctionServer.py, Gateway.py